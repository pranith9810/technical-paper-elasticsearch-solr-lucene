# Elasticsearch, solr and lucene

Elasticsearch and solr are open source search engine platforms based on lucene.

## Lucene

[Lucene](https://lucene.apache.org/) is a free and open source search engine library supported by [Apache Software Foundation](https://www.apache.org/). It is written in **Java** and has also been ported to other languages like python, C#, PHP etc.

It is mainly focused on **text indexing and searching**. Lucene can index any data available in textual format including HTML documents, Word documents etc as long as textual information from can be extracted from them.

It uses a data structure known as [Inverted Index](https://en.wikipedia.org/wiki/Inverted_index) which is stored as a file system or memory as a set of index files. Before the data is added to an index it is processed by an analyzer to convert the data into a fundamental search unit called as term which are added into the lucene index.

As indexing increases the speed of searching for data, lucene is pretty fast and efficient.

### Features of Lucene

- Has powerful and efficient search algorithms

- **Calculates a score** for each document returned and ranks it based on the score

- Allows searching and indexing simultaneously.

- Search behavior can be extended by sorting, filtering etc.

## Elasticsearch

[Elastisearch](https://www.elastic.co/) is highly scalable, open source text search and analytics engine based on the lucene library. It was developed using JAVA. Parts of the software are licensed under various open source licenses like Apache License and parts of it come under a proprietary Elastic license.

It is the most popular search engine and ranked number one in [DB-Engines ranking](https://en.wikipedia.org/wiki/DB-Engines_ranking).

Elasticsearch can be used to search in all kinds of documents. It has the capability of storing, searching and analyzing the data in real time. It uses JSON based REST api to refer to Lucene features. It is schema-less, using some defaults to index the data.

Elasticsearch is considered as a NoSQL database as:

- it is easy to use

- has a great community

- Compatibility with JSON etc.

### Backend components of Elasticsearch

#### Node

It is a single server in a cluster which takes part in the cluster's searching capabilities.

#### Cluster

It is a collection of nodes. Elasticsearch works in a distributed environment with cross cluster replication.

#### Index

Index is a collection of documents that have similar characteristics. It is similar to database in RDBMS. Within a cluster we can store many indexes.

#### Document

Document is a basic unit that can be indexed. Within an index we can store as many documents as we want.

#### Shards

Elasticsearch allows us to divide index into multiple indexes called shards. It allows us to horizontally split the data volume which increases the performance.

#### The Elastic Stack

Although elasticsearch is a search engine at its core we can also visualize data. Elasticsearch, Logstash and Kibana are the main components of The Elastic Stack also known as *ELK*.

## Solr

[Apache Solr](https://lucene.apache.org/solr/) is an open source enterprise-search platform built upon the Lucene library. It is written in JAVA. It is the second most popular search engine according to the [DB-Engines ranking](https://en.wikipedia.org/wiki/DB-Engines_ranking).

It is a standalone full text search engine and has REST-like HTTP/XML and JSON APIs which makes it usable with most of the programming languages.

### Features of Solr

- Full text search

- Database Integration

- NoSQL Features

- Rich document handling etc.

### Sequence of operations

In order to search a document, the following operations happen in sequence:

1. Indexing: It converts the documents into a machine-readable format which is called Indexing.

2. Querying: It tries to understand the terms of a query asked by the user.

3. Mapping: it maps the user query to the documents stored in the database to find the appropriate result.

4. Ranking the outcome: It ranks the outputs as per their relevance.

## Solr vs Elasticsearch

### Community and Open source

Both have a very active community. Both are open source but they have a small difference i.e Solr is truly open source whereas in Elasticsearch people can still contribute to it but it is up to Elastic whether to accept it or not.

### Core technology

Solr and Elasticsearch both are built upon Lucene so the core functionality of both are same.

### Scalability

In terms of scalability Elasticsearch is winning the game but recently SolrCloud was released which made it easier to scale a cluster in easier and a faster way compared to the older versions of Solr. Still Solr has a long way to go but future seems bright in terms of the size of the datasets solr can handle.

### Performance

Performance is almost same in both the engines and both have solid performance.

### Visualization

Solr is primarily focused on text search and seems to handle it very well. Elasticsearch has gone beyond search to handle visualization with the Elastic Stack.

### Data Format

Elasticsearch is compatible with JSON. Solr was originally aimed at XML but it supports JSON as well.

## References

- [Lucene wikipedia page](https://en.wikipedia.org/wiki/Apache_Lucene)

- [Using apache lucene to search text](https://www.ibm.com/developerworks/library/os-apache-lucenesearch/index.html)

- [Elasticsearch wikipedia page](https://en.wikipedia.org/wiki/Elasticsearch)

- [Introduction to Elasticsearch](https://towardsdatascience.com/an-overview-on-elasticsearch-and-its-usage-e26df1d1d24a)

- [Solr wikipedia page](https://en.wikipedia.org/wiki/Apache_Solr)

- [Solr vs Elasticsearch](https://www.searchtechnologies.com/blog/solr-vs-elasticsearch-top-open-source-search)
